/**
 * For working with RoadMap
 */
var roadmap = function(){
	var cookieid = 'roadmap';
	var projectid = _projectname();

	// save in cookie
	function _save(toSave)
	{
		var projectname = _projectname();
		var _data = _getCookies();
		var _toSave = {};
		_toSave[projectname] = toSave;

		if(!_data[projectname]) _data[projectname] = {};

		_data[projectname] = jQuery.extend(true, _data[projectname], _toSave[projectname]);
		jsCookies.set(cookieid, JSON.stringify(_data));
	}

	// получить данные из куки
	function _getCookies()
	{
		var data = jsCookies.get(cookieid);
		
		if(!data || data=='')
		{
			data = {};
		}
		else
		{
			data = JSON.parse(data);
		}
		return data;
	}

    // gets project`s name from #current_project
	function _projectname()
	{
		return jQuery('#current_project').attr('project-name');
	}
	function _projectid()
	{
		return jQuery('#current_project').attr('project-id');
	}

    // check & run
	function _ifTablesHided(data, version, projectname)
	{
		if(data[projectname][version]['roadmap.hideTable'])
		{
			if(data[projectname][version]['roadmap.hideTable']['value']=='hide')
			{
				roadmap.toggleBlock('hide', version);
				return true;
			}
		}
		return false;
	}

	function _ifAllStatusHidded(data, version, projectname)
	{
		if(data[projectname][version]['toggleAllStatus'])
		{
			if(data[projectname][version]['toggleAllStatus']['value']=='hide')
			{
				roadmap.toggleStatusBlock('hide', version);
				return true;
			}
		}
		return false;
	}

	function _ifByStatusHidded(data, version, projectname)
	{
		if(data[projectname][version]['toggleByStatus'])
		{
			for(var status in data[projectname][version]['toggleByStatus'])
			{
				if(data[projectname][version]['toggleByStatus'][status]['value']=='hide')
				{
					roadmap.toggleStatusBlock('hide', version, status);
				}
			}
		}
		return false;
	}

    // Изменение всета кнопочки "все" у блоков
    function _toToggleAllStatusColor(version)
    {
        var obj = jQuery('.toggle_versions_allstatus[versionid="'+version+'"]');
        var visibleLength = _getLegthStatusVisible(version);
        if( visibleLength > 0)
        {
            obj.removeClass('show_version_status').addClass('hide_version_status');
        }
        else
        {
            obj.removeClass('hide_version_status').addClass('show_version_status');
        }
    }

    // Получить кол-во блоков спрятанных объектов 
    function _getLegthStatusVisible(version)
    {
        return jQuery('.versions_total_statuses_message[versionid="'+version+'"]:visible').length;
    }

    function _getLegthStatus(version)
    {
        return jQuery('.versions_total_statuses_message[versionid="'+version+'"]').length;
    }

	return {
		saveincookie: function(functionName, value, version, status)
		{
			var _toSave = {};
			_toSave[version] = {};
			_toSave[version][functionName] = {};
			_statData = {"value":value, "version":version, "functionName":functionName};
			if(status)
			{
				_toSave[version][functionName][status] = _statData;
			}
			else
			{
				_toSave[version][functionName] = _statData;
			}
			// console.log('saveincookie; functionName:'+functionName+'; value:'+value+';')
			// console.log('saveincookie;', _toSave);
			_save(_toSave)
		}
		, removefromcookie: function(version, functionName)
		{
			var data =_getCookies();
			var projectname = _projectname();
			// console.log('removeing: '+version+' -> '+functionName, data);
			if(data[projectname])
			{
				// console.log('removeing: '+projectname);
				if(data[projectname][version] && data[projectname][version][functionName])
				{
					// console.log('removeing: deleting');
					data[projectname][version][functionName] = {};
					delete data[projectname][version][functionName];
					jsCookies.set(cookieid, JSON.stringify(data));
				}
			}
		}
		, toggleBlock: function(action, version, obj)
		{
			if(!obj)
			{
				obj = jQuery('.toggle_versions_table[versionid="'+version+'"]');
			}
			switch(action)
			{
				case 'hide':
					jQuery('.versions_table[versionid="'+version+'"]').hide(0, function(){
						obj.html('показать список задач');
					})
				break;
				case 'show':
					jQuery('.versions_table[versionid="'+version+'"]').show(0, function(){
						obj.html('свернуть список задач');
					})
				break;
			}
		}
		// Переключение статусов у блоков и конкретных статусов
		, toggleStatusBlock: function(action, version, status)
		{
			var srchStr = '[versionid="'+version+'"]';
			if(status)
			{
				srchStr += '[statusid="'+status+'"]';
			}
			switch(action)
			{
				case 'show':
					jQuery('tr.issues_stat'+srchStr).show(0, function(){
						jQuery('a.toggle_versions_status'+srchStr).removeClass('hide_version_status').addClass('show_version_status');
						jQuery('.versions_total_statuses_message'+srchStr).hide(0);
					})
				break;
				case 'hide':
					jQuery('tr.issues_stat'+srchStr).hide(0, function(){
						jQuery('a.toggle_versions_status'+srchStr).removeClass('show_version_status').addClass('hide_version_status');
						jQuery('.versions_total_statuses_message'+srchStr).show(0);
					})
				break;
			}
		}
		, clearall: function()
		{
			jsCookies.set(cookieid, JSON.stringify({}));
			location.reload();
		}

        // hides whole table of Obj version
        , hideTable: function(obj)
        {
            var _o = jQuery(obj);
            var _id = _o.attr('versionid');
            // console.log('func: roadmap.hideTable')
            if(jQuery('.versions_table[versionid="'+_id+'"]').is(':visible'))
            {
                roadmap.saveincookie('roadmap.hideTable','hide', _id);
                roadmap.toggleBlock('hide', _id, _o);
            }
            else
            {
                roadmap.saveincookie(arguments.callee.name,'show', _id);
                roadmap.toggleBlock('show', _id, _o);
            }
        }

        // toggles whole statuses in the table
        , toggleAllStatus: function (Obj)
        {
            var _o = jQuery(Obj);
            var verid = _o.attr('versionid');
            var visibleLength = _getLegthStatusVisible(verid);
            roadmap.removefromcookie(verid, 'toggleByStatus');
            if(visibleLength>0)
            {
                roadmap.saveincookie('toggleAllStatus','show', verid);
                roadmap.toggleStatusBlock('show',verid);
            }
            else
            {
                roadmap.saveincookie('toggleAllStatus','hide', verid);
                roadmap.toggleStatusBlock('hide',verid);
            }
            _toToggleAllStatusColor(verid);
        }

        // toggle by status of each version
        , toggleByStatus: function (Obj)
        {
            // console.log('func: roadmap.toggleByStatus')
            var _o       = jQuery(Obj);
            var verid    = _o.attr('versionid');
            var statid   = _o.attr('statusid');
            var statname = _o.attr('statusname');

            var _selfName = 'toggleByStatus';

            roadmap.removefromcookie(verid, 'toggleAllStatus');
            if( jQuery('tr.issues_stat[versionid="'+verid+'"][statusid="'+statid+'"]:first').is(':visible') )
            {
                roadmap.saveincookie(_selfName,'hide', verid, statid);
                roadmap.toggleStatusBlock('hide',verid,statid);
                // console.log('func: roadmap.toggleByStatus -> hide')
            }
            else
            {
                roadmap.saveincookie(_selfName,'show', verid, statid);
                roadmap.toggleStatusBlock('show',verid,statid);

                // console.log('func: roadmap.toggleByStatus -> show')

                if(_getLegthStatusVisible(verid)>0)
                {
                    jQuery('.versions_total_statuses_message[versionid="'+verid+'"]:visible').each(function(){
                        var _status = jQuery(this).attr('statusid');
                        var _version = jQuery(this).attr('versionid');
                        roadmap.saveincookie(_selfName,'hide', _version, _status);
                    })
                }
            }
            _toToggleAllStatusColor(verid);

            // Если ручками все выключаенися, значит сохранить надо toggleAllStatus
            if(_getLegthStatusVisible(verid)==_getLegthStatus(verid))
            {
                // console.log('func: roadmap.toggleByStatus -> toggleAllStatus')
                roadmap.removefromcookie(verid, arguments.callee.name);
                roadmap.saveincookie('toggleAllStatus','hide', verid);
            }
        }

        // runs after document ready.
		, init: function()
		{
			var projectname = _projectname();
			// console.log('==== init');
			var data = _getCookies();
			// console.log('cookie data', data);
			if(data[projectname])
			{
				for(var version in data[projectname])
				{
                    // if tables hidded
					if(!_ifTablesHided(data, version, projectname))
					{
						if(!_ifAllStatusHidded(data, version, projectname))
						{
							_ifByStatusHidded(data, version, projectname);
						}
					}
				}
			}
			
		}
	}
}();

// Получить данные о проекте
var project = {
	name: function(){
		return jQuery('#current_project').attr('project-name');
	},
	id: function(){
		return jQuery('#current_project').attr('project-id');
	}
};
