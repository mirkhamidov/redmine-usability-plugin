require 'issues_helper_patch'
require 'versions_controller_patch'
require 'previews_controller_patch'
require 'application_helper_patch'

Redmine::Plugin.register :redmine_usability do
  name 'Redmine Usability plugin'
  author 'Jasur Mirkhamidov'
  description 'This is a plugin for Redmine'
  version '0.2.1'
  url 'https://bitbucket.org/mirkhamidov/redmine-usability-plugin'
end
