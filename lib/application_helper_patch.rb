require_dependency 'application_helper'

module ApplicationHelperPatch
    def self.included(base) # :nodoc:
        base.send(:include, InstanceMethods)
        
        base.class_eval do
          alias_method_chain :parse_redmine_links, :mine
        end
    end

    module InstanceMethods
        def parse_redmine_links_with_mine(text, default_project, obj, attr, only_path, options)
            # Так вызываем родительский метод без включения текущего метода ;)
            b = parse_redmine_links_without_mine(text, default_project, obj, attr, only_path, options)
            return b
        end

        def return_custom_field_value(issue, fieldid)
            s = ''
            issue.custom_values.each do |cast_val| 
                if(cast_val[:custom_field_id]==fieldid) 
                    if(cast_val[:value])
                        s << cast_val[:value]
                    end
                end 
            end 
            s.html_safe
        end
    end
end

ApplicationHelper.send(:include, ApplicationHelperPatch)