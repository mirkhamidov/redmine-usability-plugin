require_dependency 'versions_controller'

module VersionsControllerPatch
  
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    
    base.class_eval do
        helper :issues
        include IssuesHelper
        alias_method_chain :index, :mine
        alias_method_chain :show, :mine
    end
  end

  module InstanceMethods

    def show_with_mine

        respond_to do |format|
            format.html {
                @issues = @version.fixed_issues.visible.
                    includes(:status, :tracker, :priority).
                    # reorder("#{Tracker.table_name}.position, #{Issue.table_name}.id").
                    # reorder("#{Issue.table_name}.status_id asc, #{Project.table_name}.lft, #{Tracker.table_name}.position").
                    reorder("#{Issue.table_name}.lft asc").
                    all
            }
          format.api
        end
    end

    def index_with_mine
      respond_to do |format|
        format.html {
          @trackers = @project.trackers.sorted.all
          retrieve_selected_tracker_ids(@trackers, @trackers.select {|t| t.is_in_roadmap?})
          @with_subprojects = params[:with_subprojects].nil? ? Setting.display_subprojects_issues? : (params[:with_subprojects] == '1')
          project_ids = @with_subprojects ? @project.self_and_descendants.collect(&:id) : [@project.id]

          @versions = @project.shared_versions || []
          @versions += @project.rolled_up_versions.visible if @with_subprojects
          @versions = @versions.uniq.sort
          unless params[:completed]
            @completed_versions = @versions.select {|version| version.closed? || version.completed? }
            @versions -= @completed_versions
          end

          @issues_by_version = {}
          @statuses = IssueStatus.all
          if @selected_tracker_ids.any? && @versions.any?
            issues = Issue.visible.
              includes(:project, :tracker).
              preload(:status, :priority, :fixed_version).
              where(:tracker_id => @selected_tracker_ids, :project_id => project_ids, :fixed_version_id => @versions.map(&:id)).
              # order("#{Issue.table_name}.status_id asc, #{Project.table_name}.lft, #{Tracker.table_name}.position")
              order("#{Issue.table_name}.lft asc")
            @issues_by_version = issues.group_by(&:fixed_version)
          end
          @versions.reject! {|version| !project_ids.include?(version.project_id) && @issues_by_version[version].blank?}
        }
        format.api {
          @versions = @project.shared_versions.all
        }
      end
    end
  end
end

VersionsController.send(:include, VersionsControllerPatch)
