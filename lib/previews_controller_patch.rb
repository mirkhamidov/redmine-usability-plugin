require_dependency 'previews_controller'

module PreviewsControllerPatch
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    
    base.class_eval do
    end
  end

  module InstanceMethods
    def doc
        # if params[:id].present? && document = Document.visible.find_by_id(params[:id])
        #     @previewed = document
        # end
        @text = (params[:document] ? params[:document][:description] : nil)
        render :partial => 'common/preview'
    end
  end
end

PreviewsController.send(:include, PreviewsControllerPatch)
