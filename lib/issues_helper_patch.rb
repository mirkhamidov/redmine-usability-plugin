#this is extends defs in issues_helper.rb
require_dependency 'issues_helper'
require_dependency 'issue_priority'

module IssuesHelperPatch
  
  def self.included(base) # :nodoc:

    base.send(:include, InstanceMethods)
    
    base.class_eval do
      alias_method_chain :render_descendants_tree, :mine
      
    end
    
  end

  module InstanceMethods

    Issue.safe_attributes 'grorder', :if => lambda {|issue, user| issue.new_record? || user.allowed_to?(:edit_issues, issue.project) }


    def render_descendants_tree_with_mine(issue)
      reset_cycle
      s = '<form><table class="list issues">'
      # s << YAML::dump(issue)
      # if (issue.parent_issue_id!=nil)
        # s << debug(issue.parent_issue_id)
      # end
      # s << '<hr>'
      # s << debug(issue)
      # s << debug(issue_fields_rows)
      # s << '<hr>'
      
      # s << '<hr>'
      # s << debug(issue.descendants.visible)
      # s << '<hr>'
      # s << debug(issue.descendants.visible.sort_by(&:lft))
      # s << issue.inspect
      # issue_list(issue.descendants.visible.sort_by(&:lft)) do |child, level|
      issue_list(issue.descendants.visible.sort_by{|e| [e.lft]}) do |child, level|
        # s << debug(child.visible_custom_field_values)
        # s << '<hr>'
        css = "issue issue-#{child.id} hascontextmenu "
        css << cycle('odd', 'even')
        css << " "
        css << " idnt idnt-#{level} " if level > 0
        css << child.css_classes
        s << content_tag('tr',
               content_tag('td', check_box_tag("ids[]", child.id, false, :id => nil), :class => 'checkbox') +
               content_tag('td', child.grorder) +
               # content_tag('td', "#{child.grorder} - #{child.lft}") +
               content_tag('td', child.fixed_version) +
               content_tag('td', link_to_issue(child, :truncate => 60, :project => (issue.project_id != child.project_id)), :class => 'subject') +
               content_tag('td', h(child.status)) +
               content_tag('td', link_to_user(child.assigned_to)) +
               content_tag('td', progress_bar(child.done_ratio, :width => '80px')),
               :class => css)
      end
      s << '</table></form>'
      s.html_safe
    end
  end
end

IssuesHelper.send(:include, IssuesHelperPatch)
