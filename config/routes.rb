# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

RedmineApp::Application.routes.draw do
    match '/doc/preview', :controller => 'previews', :action => 'doc', :as => 'preview_documents', :via => [:get, :post, :put]
end